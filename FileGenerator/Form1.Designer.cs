﻿namespace FileGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.create = new System.Windows.Forms.Button();
            this.lbl_entities = new System.Windows.Forms.Label();
            this.txtEntities = new System.Windows.Forms.RichTextBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // create
            // 
            this.create.Location = new System.Drawing.Point(467, 482);
            this.create.Name = "create";
            this.create.Size = new System.Drawing.Size(75, 23);
            this.create.TabIndex = 0;
            this.create.Text = "Create";
            this.create.UseVisualStyleBackColor = true;
            this.create.Click += new System.EventHandler(this.Create_Click);
            // 
            // lbl_entities
            // 
            this.lbl_entities.AutoSize = true;
            this.lbl_entities.Location = new System.Drawing.Point(12, 9);
            this.lbl_entities.Name = "lbl_entities";
            this.lbl_entities.Size = new System.Drawing.Size(39, 13);
            this.lbl_entities.TabIndex = 24;
            this.lbl_entities.Text = "Entites";
            // 
            // txtEntities
            // 
            this.txtEntities.Location = new System.Drawing.Point(57, 6);
            this.txtEntities.Name = "txtEntities";
            this.txtEntities.Size = new System.Drawing.Size(485, 96);
            this.txtEntities.TabIndex = 25;
            this.txtEntities.Text = "Alpha : {Id=int,Firstname=string,Lastname=string,password=string}\nBeta : {Id=int," +
    "Firstname=string,Lastname=string,password=string}\nCharli : {Id=int,Firstname=str" +
    "ing,Lastname=string,password=string}";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(57, 130);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(472, 304);
            this.checkedListBox1.TabIndex = 26;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 517);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.txtEntities);
            this.Controls.Add(this.lbl_entities);
            this.Controls.Add(this.create);
            this.Name = "Form1";
            this.Text = "File Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button create;
        private System.Windows.Forms.Label lbl_entities;
        private System.Windows.Forms.RichTextBox txtEntities;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
    }
}

