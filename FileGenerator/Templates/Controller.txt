﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Recipe.Core.Base.Generic;
using SMSA.API.Infrastructure;
using SMSA.Core.Attribute;
using SMSA.Core.Constant;
using SMSA.Core.DTO;
using SMSA.Core.Entity;
using SMSA.Core.Enum;
using SMSA.Core.IService;

namespace SMSA.API.Controller
{
    [CustomAuthorize]
    [CanAccess(UserPermissions.SystemUsers)]
    [RoutePrefix("<<Entity>>")]
    public class <<Entity>>Controller : Controller<I<<Entity>>Service, <<Entity>>DTO, <<Entity>>, int>
    {
        public <<Entity>>Controller(I<<Entity>>Service service) : base(service)
        {
        }

		[HttpPost]
        [Route("PostData")]
        public async Task<ResponseMessageResult> PostData(<<Entity>>DTO dtoObject)
        {
            var result = await Service.CreateAsync(dtoObject);
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }

        [HttpGet]
        [Route("GetData")]
        public async Task<ResponseMessageResult> GetData()
        {
            var result = await Service.GetAllAsync();
            if (result != null)
            {
                return await this.JsonApiSuccessAsync(Message.SuccessResult, result);
            }

            return await this.JsonApiNoContentAsync(Message.NoContent, null);
        }
    }
}