﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileGenerator
{
    public static class Constant
    {
        public static class Filename
        {
            public const string Entity = "\\{0}.cs";
            public const string DTO = "\\{0}DTO.cs";
            public const string IRepository = "\\I{0}Repository.cs";
            public const string Repository = "\\{0}Repository.cs";
            public const string IService = "\\I{0}Service.cs";
            public const string Service = "\\{0}Service.cs";
            public const string Controller = "\\{0}Controller.cs";
            public const string UnitOfWork = "\\UnitOfWork.cs";
            public const string IUnitOfWork = "\\ISMSAUnitOfWork.cs";
            public const string UnityConfig = "\\unity.config";
        }

        public static class UnitOfWork
        {
            public const string UOW1 = "private readonly I<<Entity>>Repository <<entity>>Repository;\n//// UOW1";
            public const string UOW2 = "\nI<<Entity>>Repository <<entity>>Repository, //// UOW2";
            public const string UOW3 = "this.<<entity>>Repository = <<entity>>Repository;\n//// UOW3";
            public const string UOW4 = "public I<<Entity>>Repository <<Entity>>Repository{get{return this.<<entity>>Repository;}}\n//// UOW4";

            public const string IUOW = "I<<Entity>>Repository <<Entity>>Repository { get; }\n\n//// IUOW";
        }


        public static class UnityConfig
        {
            public const string UC1 = "<alias alias=\"I<<Entity>>Repository\" type=\"SMSA.Core.IRepository.I<<Entity>>Repository, SMSA.Core\" />" +
                                      "\n<alias alias =\"<<Entity>>Repository\" type=\"SMSA.Repository.<<Entity>>Repository, SMSA.Repository\" />\n\n<!-- UC1 -->";
            public const string UC2 = "<alias alias=\"I<<Entity>>Service\" type=\"SMSA.Core.IService.I<<Entity>>Service, SMSA.Core\" />" +
                                      "\n<alias alias =\"<<Entity>>Service\" type=\"SMSA.Service.<<Entity>>Service, SMSA.Service\" />\n\n<!-- UC2 -->";
            public const string UC3 = "<register type=\"I<<Entity>>Repository\" mapTo=\"<<Entity>>Repository\" />\n<!-- UC3 -->";
            public const string UC4 = "<register type=\"I<<Entity>>Service\" mapTo=\"<<Entity>>Service\" />\n<!-- UC4 -->";
        }

        public static class Template
        {
            public const string EntityFile = "\\Templates\\Entity.txt";
            public const string DTOFile = "\\Templates\\DTO.txt";
            public const string IRepositoryFile = "\\Templates\\IRepository.txt";
            public const string RepositoryFile = "\\Templates\\Repository.txt";
            public const string IServiceFile = "\\Templates\\IService.txt";
            public const string ServiceFile = "\\Templates\\Service.txt";
            public const string ControllerFile = "\\Templates\\Controller.txt";
        }

        public static class Placeholder
        {
            public const string Entity = "<<Entity>>";
            public const string Props1 = "<<Props1>>";
            public const string Props2 = "<<Props2>>";
            public const string Props3 = "<<Props3>>";
        }

        public static class DTOProperties
        {
            public const string Property = "public {0} {1} {2} get; set; {3}\n\n";
            public const string ConvertToEntity = "entity.{0} = this.{0};\n";
            public const string ConvertFromEntity = "this.{0} = entity.{0};\n";
        }

        public static class Message
        {
            public const string AlreadyExist = "{0} Already Exists";
        }
    }

    public static class StringExtension
    {
        public static string ToCamelCase(this string str)
        {
            if (!string.IsNullOrEmpty(str) && str.Length > 1)
            {
                return Char.ToLowerInvariant(str[0]) + str.Substring(1);
            }
            return str;
        }
        
        public static Dictionary<string, string> ConvertToKeyValue(this string str)
        {
            var result = str.Trim('{', '}')
                 .Split(',')
                 .Select(x => x.Split('='))
                 .ToDictionary(x => x[0], x => x[1]);

            var a = string.Join("\r\n", result.Select(x => x.Key + " : " + x.Value));

            return result;
        }

        public static List<string> GenerateProperties (string json)
        {

            //string str = "{Id=int,Firstname=string,Lastname=string,password=string}";
            List<string> DTOProps = new List<string>();

            var pair = json.ConvertToKeyValue();
            var properties = string.Empty;
            var CTE = string.Empty;
            var CFE = string.Empty;

            
            foreach (var prop in pair)
            {
                properties += string.Format(Constant.DTOProperties.Property, prop.Value, prop.Key, "{", "}");
                CTE += string.Format(Constant.DTOProperties.ConvertFromEntity, prop.Key);
                CFE += string.Format(Constant.DTOProperties.ConvertToEntity, prop.Key);
            }

            DTOProps.Add(properties);
            DTOProps.Add(CTE);
            DTOProps.Add(CFE);

            return DTOProps;
        }
    }


    //private async Task<UserDTO> GetFacebookAccountAsync(string accessToken)
    //{
    //    var result = await new Core.Helper.FacebookHelper().GetAsync<dynamic>(accessToken, "me", "fields=id,name,email,first_name,last_name,age_range,birthday,gender,locale");
    //    if (result == null)
    //    {
    //        this.exceptionHelper.ThrowAPIException(HttpStatusCode.ExpectationFailed, string.Format(Message.NotFound, "User"));
    //    }

    //    var account = new UserDTO
    //    {
    //        //Id = result.id,
    //        Email = result.email,
    //        UserName = result.username,
    //        FirstName = result.first_name,
    //        LastName = result.last_name,
    //        Password = Validations.DefaultFacebookPassword
    //    };

    //    return account;
    //}


    //public class FacebookHelper
    //{
    //    public readonly HttpClient httpClient;

    //    public FacebookHelper()
    //    {
    //        this.httpClient = new HttpClient
    //        {
    //            BaseAddress = new Uri("https://graph.facebook.com/v4.0/")
    //        };

    //        this.httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //    }

    //    public async Task<T> GetAsync<T>(string accessToken, string endPoint, string args = null)
    //    {
    //        var response = await this.httpClient.GetAsync(string.Format("{0}assecc_token={1}&{2}", endPoint, accessToken, args));
    //        if (!response.IsSuccessStatusCode)
    //        {
    //            return default(T);
    //        }

    //        var result = await reponse.content.ReadAsStringAsync();
    //        return JsonConverter.DeserializeObject<T>(result);
    //    }
    //}


    /*
    TODO: Input box will convert to json
    {"Alpha":[{"int":"Id"},{"string":"Firstname"},{"string":"Lastname"}],"Bravo":[{"int":"Id"},{"string":"Firstname"},{"string":"Lastname"}]}
    
    */

}
