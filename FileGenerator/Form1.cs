﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileGenerator
{
    public partial class Form1 : Form
    {
        public string Base = @"D:\.Net Projects\Testing\src";

        public string EntityPath = string.Empty;
        public string DTOPath = string.Empty;
        public string IRepositoryPath = string.Empty;
        public string IServicePath = string.Empty;
        public string RepositoryPath = string.Empty;
        public string ServicePath = string.Empty;
        public string ControllerPath = string.Empty;
        public string IUnitOfWorkPath = string.Empty;
        public string UnitOfWorkPath = string.Empty;
        public string UnityConfigPath = string.Empty;
        public string entityName = string.Empty;

        public string apiCSProj = string.Empty;

        public List<string> props = new List<string>();

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        public void Init()
        {
            EntityPath = Base + "\\SMSA.Core\\Entity";
            DTOPath = Base + "\\SMSA.Core\\DTO";
            IRepositoryPath = Base + "\\SMSA.Core\\IRepository";
            IServicePath = Base + "\\SMSA.Core\\IService";

            RepositoryPath = Base + "\\SMSA.Repository";
            ServicePath = Base + "\\SMSA.Service";

            ControllerPath = Base + "\\SMSA.API\\Controller";

            IUnitOfWorkPath = Base + "\\SMSA.Core\\IRepository";
            UnitOfWorkPath = Base + "\\SMSA.Repository";
            UnityConfigPath = Base + "\\SMSA.API";

            apiCSProj = Base + "\\SMSA.API\\SMSA.API.csproj";

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(ControllerPath);
            System.IO.FileSystemInfo[] files = di.GetFileSystemInfos();
            checkedListBox1.Items.AddRange(files);
        }
        
        private void Create_Click(object sender, EventArgs e)
        {
            var a = checkedListBox1.CheckedItems;

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    string str = (string)((System.IO.FileInfo)checkedListBox1.Items[i]).Name;
                    MessageBox.Show(str);
                }
            }

            string entitiesText = txtEntities.Text;

            if (!string.IsNullOrEmpty(entitiesText))
            {
                var entities = entitiesText.Replace(" ", string.Empty)
                    .Split(new[] { "\n" }, StringSplitOptions.None)
                    .Select(x => x.Split(':'))
                    .ToDictionary(x => x[0], x => x[1]);

                foreach (var entity in entities)
                {
                    entityName = entity.Key;
                    props = StringExtension.GenerateProperties(entity.Value);
                    CreateFiles();
                }
            }
            else
            {
                MessageBox.Show("Please Enter names");
            }
        }
        
        // Generate Files
        private void CreateFiles()
        {
            //CreateEntity();
            //CreateDTO();
            //CreateIRepository();
            //CreateRepository();
            //CreateIService();
            //CreateService();
            //CreateController();

            //Update Unit OF Work
            //IUnitOfWork();
            //UnitofWork();
            //UnityConfig();

            //DeleteContrller();
        }

        private void CreateEntity()
        {
            var path = EntityPath + String.Format(Constant.Filename.Entity, entityName);

            if (!File.Exists(path))
            {
                string entityFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.EntityFile;

                string text = File.ReadAllText(entityFile);
                text = text.Replace(Constant.Placeholder.Props1, props[0]);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateDTO()
        {
            var path = DTOPath + String.Format(Constant.Filename.DTO, entityName);

            if (!File.Exists(path))
            {
                string dtoFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.DTOFile;
                
                string text = File.ReadAllText(dtoFile);
                text = text.Replace(Constant.Placeholder.Props1, props[0]);
                text = text.Replace(Constant.Placeholder.Props2, props[1]);
                text = text.Replace(Constant.Placeholder.Props3, props[2]);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateIRepository()
        {
            var path = IRepositoryPath + String.Format(Constant.Filename.IRepository, entityName);

            if (!File.Exists(path))
            {
                string iRepoFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.IRepositoryFile;

                string text = File.ReadAllText(iRepoFile);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateRepository()
        {
            var path = RepositoryPath + String.Format(Constant.Filename.Repository, entityName);

            if (!File.Exists(path))
            {
                string repoFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.RepositoryFile;

                string text = File.ReadAllText(repoFile);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateIService()
        {
            var path = IServicePath + String.Format(Constant.Filename.IService, entityName);

            if (!File.Exists(path))
            {
                string serFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.IServiceFile;

                string text = File.ReadAllText(serFile);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateService()
        {
            var path = ServicePath + String.Format(Constant.Filename.Service, entityName);

            if (!File.Exists(path))
            {
                string serFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.ServiceFile;

                string text = File.ReadAllText(serFile);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
        }

        private void CreateController()
        {
            var path = ControllerPath + String.Format(Constant.Filename.Controller, entityName);

            if (!File.Exists(path))
            {
                string conFile = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + Constant.Template.ControllerFile;

                string text = File.ReadAllText(conFile);
                text = text.Replace(Constant.Placeholder.Entity, entityName);
                File.WriteAllText(path, text);
            }
            else
            {
                MessageBox.Show(string.Format(Constant.Message.AlreadyExist, entityName));
            }
            UpdateCSProjFiles();
        }

        private void UnitofWork()
        {
            var path = UnitOfWorkPath + Constant.Filename.UnitOfWork;

            var UOW1 = Constant.UnitOfWork.UOW1.Replace("<<Entity>>", entityName);
            var UOW2 = Constant.UnitOfWork.UOW2.Replace("<<Entity>>", entityName);
            var UOW3 = Constant.UnitOfWork.UOW3.Replace("<<Entity>>", entityName);
            var UOW4 = Constant.UnitOfWork.UOW4.Replace("<<Entity>>", entityName);
            var camlName = entityName.ToCamelCase();

            UOW1 = UOW1.Replace("<<entity>>", camlName);
            UOW2 = UOW2.Replace("<<entity>>", camlName);
            UOW3 = UOW3.Replace("<<entity>>", camlName);
            UOW4 = UOW4.Replace("<<entity>>", camlName);

            string fileText = File.ReadAllText(path);
            fileText = fileText.Replace("//// UOW1", UOW1);
            fileText = fileText.Replace("//// UOW2", UOW2);
            fileText = fileText.Replace("//// UOW3", UOW3);
            fileText = fileText.Replace("//// UOW4", UOW4);
            File.WriteAllText(path, fileText);
        }

        private void IUnitOfWork()
        {
            var path = IUnitOfWorkPath + Constant.Filename.IUnitOfWork;

            var IUOW = Constant.UnitOfWork.IUOW.Replace("<<Entity>>", entityName);

            string fileText = File.ReadAllText(path);
            fileText = fileText.Replace("//// IUOW", IUOW);
            File.WriteAllText(path, fileText);
        }

        private void UnityConfig()
        {
            var path = UnityConfigPath + Constant.Filename.UnityConfig;

            var UC1 = Constant.UnityConfig.UC1.Replace("<<Entity>>", entityName);
            var UC2 = Constant.UnityConfig.UC2.Replace("<<Entity>>", entityName);
            var UC3 = Constant.UnityConfig.UC3.Replace("<<Entity>>", entityName);
            var UC4 = Constant.UnityConfig.UC4.Replace("<<Entity>>", entityName);

            string fileText = File.ReadAllText(path);
            fileText = fileText.Replace("<!-- UC1 -->", UC1);
            fileText = fileText.Replace("<!-- UC2 -->", UC2);
            fileText = fileText.Replace("<!-- UC3 -->", UC3);
            fileText = fileText.Replace("<!-- UC4 -->", UC4);
            File.WriteAllText(path, fileText);
        }

        private void UpdateCSProjFiles()
        {
            var path = apiCSProj;

            var txt = "<Compile Include=\"Controller\\" + entityName + "Controller.cs\" />\n<!--csproj-->";

            string fileText = File.ReadAllText(path);
            fileText = fileText.Replace("<!--csproj-->", txt);
            File.WriteAllText(path, fileText);
        }

        private void DeleteContrller()
        {
            var csprojFilePath = apiCSProj;
            var apiFolderPath = ControllerPath;

            // Files to be deleted    
            string authorsFile = entityName + "Controller.cs";

            File.WriteAllLines(csprojFilePath, File.ReadLines(csprojFilePath).Where(l => !l.Contains(authorsFile)).ToList());
            try
            {
                // Check if file exists with its full path    
                if (File.Exists(Path.Combine(apiFolderPath, authorsFile)))
                {
                    // If file found, delete it    
                    File.Delete(Path.Combine(apiFolderPath, authorsFile));
                    Console.WriteLine("File deleted.");
                }
                else Console.WriteLine("File not found");
            }
            catch (IOException ioExp)
            {
                Console.WriteLine(ioExp.Message);
            }
        }
    }
}
